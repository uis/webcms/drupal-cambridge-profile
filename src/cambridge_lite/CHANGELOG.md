Changelog
=========

2.10.7

No changes currently - only in the full profile.


2.10.6

No changes currently - only in the full profile.


2.10.5

No changes currently - only in the full profile.


2.10.4

No changes currently - only in the full profile.


2.10.3

No changes currently - only in the full profile.


2.10.2

No changes currently - only in the full profile.


2.10.1

No changes currently - only in the full profile.


2.10

No changes currently - only in the full profile. 


2.9.3

Upgraded the Cambridge Theme to [1.25] (https://gitlab.developers.cam.ac.uk/uis/webcms/drupal-cambridge-theme/-/tags/7.x-1.25)
Upgraded the Cambridge Teasers module to [1.10] (https://gitlab.developers.cam.ac.uk/uis/devops/webcms/drupal-7-features/cambridge_teasers/-/releases/7.x-1.10)


2.9.2

Upgraded the Cambridge Theme to [1.24] (https://gitlab.developers.cam.ac.uk/uis/webcms/drupal-cambridge-theme/-/tags/7.x-1.24)
Upgraded the Cambridge Teasers module to [1.9] (https://gitlab.developers.cam.ac.uk/uis/devops/webcms/drupal-7-features/cambridge_teasers/-/tags/7.x-1.9)


2.9.1

The following modules have been upgraded:

Upgraded the Imagecrop module to 1.1 (https://gitlab.developers.cam.ac.uk/uis/devops/webcms/drupal-modules/imagecrop/-/releases/1.1)

The following modules have had patches added:

Link module has had a patch added to resolve the removal of query strings issue (https://www.drupal.org/files/issues/link_7.x-1.x-undo-query-string-removal-1321482.patch)


2.9

Upgraded the Cambridge Theme to [1.23] (https://gitlab.developers.cam.ac.uk/uis/webcms/drupal-cambridge-theme/-/tags/7.x-1.23)
Upgraded the Raven module to [1.7] (https://gitlab.developers.cam.ac.uk/uis/webcms/drupal-modules/raven/-/tags/1.7)
Upgraded the Simple Lookup module to [1.3] (https://gitlab.developers.cam.ac.uk/uis/webcms/drupal-modules/simple_lookup/-/releases/1.3)

The following modules and libraries have been upgraded with security releases:

Upgraded JQuery Update to [4.1] (https://www.drupal.org/project/jquery_update/releases/7.x-4.1)

The following modules have been upgraded with maintenance or scheduled releases:

Upgraded CTools to [1.21] (https://www.drupal.org/project/ctools/releases/7.x-1.21)
Upgraded Features to [2.15] (https://www.drupal.org/project/features/releases/7.x-2.15)
Upgraded Views to [3.29] (https://www.drupal.org/project/views/releases/7.x-3.29)


2.8

5 May 2022

Upgraded the Cambridge Theme to [1.18] (https://github.com/misd-service-development/drupal-cambridge-theme/releases/tag/7.x-1.18)

The following modules have security upgrades:

Upgraded Link to [1.11] (https://www.drupal.org/project/link/releases/7.x-1.11)

The following modules have been upgraded with maintenance or scheduled releases:

Upgraded CTools to [1.20] (https://www.drupal.org/project/ctools/releases/7.x-1.20)
Upgraded Features to [2.14] (https://www.drupal.org/project/features/releases/7.x-2.14)
Upgraded Menu Block to [2.9] (https://www.drupal.org/project/menu_block/releases/7.x-2.9)
Upgraded Token to [1.9] (https://www.drupal.org/project/token/releases/7.x-1.9)
Upgraded Views to [3.25] (https://www.drupal.org/project/views/releases/7.x-3.25)


2.7

20 November 2020

Upgraded the Cambridge Theme to [1.17] (https://github.com/misd-service-development/drupal-cambridge-theme/releases/tag/7.x-1.17)
Upgraded Cambridge Teasers to [1.8] (https://github.com/misd-service-development/drupal-feature-teasers/releases/tag/7.x-1.8)

The following modules have maintenance or scheduled releases:

Upgraded Chaos Tools to [1.17] (https://www.drupal.org/project/ctools/releases/7.x-1.17)
Upgraded Features to [2.13] (https://www.drupal.org/project/features/releases/7.x-2.13)


2.6 12 October 2020

The 2.6 version of the profile will not receive a formal release due to an enormous bug in the 2.12 version of the features module.


2.5 

27 March 2020

Upgraded the Cambridge Theme to [1.14] (https://github.com/misd-service-development/drupal-cambridge-theme/releases/tag/7.x-1.14)

The following modules have been added to the profile:

Added JQuery Update [2.7] (https://www.drupal.org/project/jquery_update/releases/7.x-2.7)

The following modules have maintenance or scheduled releases:

Updgraded Link to [1.7] (https://www.drupal.org/project/link/releases/7.x-1.7)
Upgraded Views to [3.24] (https://www.drupal.org/project/views/releases/7.x-3.24)

Changed imagecrop module source to University Gitlab so it can be pre-patched with patches to make it Media2 compatible. 



2.4

3 October 2019

The Raven module has been upgraded to [1.6.2] (https://gitlab.developers.cam.ac.uk/uis/webcms/drupal-modules/raven/-/tags/v1.6.2).
This new release will allow logins to Drupal sites to be restricted to members of a particular lookup group and/or lookup institution. 
The new release will also update a user's "realname" in Drupal based on the value from lookup - if it has not already been filled in on the Drupal site.
There is also a new module - Simple Lookup [1.1] (https://gitlab.developers.cam.ac.uk/uis/webcms/drupal-modules/simple_lookup/-/tags/v1.1)
The simple lookup module is a dependency for the new version of the Raven module.


The following other modules have maintenance or scheduled releases:

Upgraded Views to [3.23] (https://www.drupal.org/project/views/releases/7.x-3.23)


2.3

4 April 2019

The following modules have maintenance or scheduled releases:

Upgraded Cambridge Teasers to [1.6] (https://github.com/misd-service-development/drupal-feature-teasers/releases/tag/7.x-1.6)
Upgraded Views to [3.22] (https://www.drupal.org/project/views/releases/7.x-3.22)


2.2

21 March 2019

The following modules have been upgraded for security reasons:

Upgraded Views to [3.21] (https://www.drupal.org/project/views/releases/7.x-3.21)


2.1

25 February 2019

The following modules have been upgraded as a result of [Drupal SA-Core-2019-003] (https://www.drupal.org/sa-core-2019-003)

Upgraded Link to [1.6] (https://www.drupal.org/project/link/releases/7.x-1.6)

The following modules have other required security or maintenance upgrades:

Upgraded Theme to [1.11] (https://github.com/misd-service-development/drupal-cambridge-theme/releases/tag/7.x-1.11)

Upgraded Chaos Tools to [1.15] (https://ftp.drupal.org/files/projects/ctools-7.x-1.15.zip)
Upgraded Menu Block to [2.8] (https://www.drupal.org/project/menu_block/releases/7.x-2.8)

The following modules have been removed as they are no longer needed:

Pathauto Persist


2.0

21 November 2018

Upgraded Theme to [1.10] (https://github.com/misd-service-development/drupal-cambridge-theme/releases/tag/7.x-1.10)

Upgraded Features to [2.11] (https://www.drupal.org/project/features/releases/7.x-2.11)

Upgraded Cambridge Carousel to [1.1] (https://github.com/misd-service-development/drupal-feature-carousel/releases/tag/7.x-1.1)
Upgraded Cambridge Teasers to [1.5] (https://github.com/misd-service-development/drupal-feature-teasers/releases/tag/7.x-1.5)

7.x-1.8.1

5 October 2018

Upgraded Chaos Tools Suite to [1.14] (https://www.drupal.org/project/ctools/releases/7.x-1.14)
Upgraded Libraries API to [2.5] (https://www.drupal.org/project/libraries/releases/7.x-2.5)
Upgraded Views to [3.20] (https://www.drupal.org/project/views/releases/7.x-3.20)

7.x-1.8

22 May 2017

Upgraded Theme to [1.8] (https://github.com/misd-service-development/drupal-cambridge-theme/)

Upgraded Chaos Tools Suite to [1.12] (https://www.drupal.org/project/ctools/releases/7.x-1.12)
Upgraded Token to [1.7] (https://www.drupal.org/project/token/releases/7.x-1.7)
Upgraded Views to [3.16] (https://www.drupal.org/project/views/releases/7.x-3.16)

7.x-1.7.6

10 May 2017

Upgraded Cambridge Theme to [1.7] (https://github.com/misd-service-development/drupal-cambridge-theme/releases/tag/7.x-1.6).


7.x-1.7.5

25 April 2017

Upgraded Cambridge Theme to [1.6] (https://github.com/misd-service-development/drupal-cambridge-theme/releases/tag/7.x-1.6).


7.x-1.7.4

20 January 2017

No changes. Changes in this release are only in the full version of the Profile.


7.x-1.7.3

19 January 2017

No changes. Changes in this release are only in the full version of the Profile.


7.x-1.7.2

6 January 2017

Upgraded Cambridge Theme to [1.5] (https://github.com/misd-service-development/drupal-cambridge-theme/releases/tag/7.x-1.5).


7.x-1.7.1

29 December 2016

Patched the link module with the fix for issue [#2666912](https://www.drupal.org/node/2666912)


7.x-1.7
-------

10 November 2016.

Upgrades Chaos Tools Suite to [1.11] (https://www.drupal.org/project/ctools/releases/7.x-1.11). 
Upgraded Features to [2.10] (https://www.drupal.org/project/features/releases/7.x-2.10). 
Upgraded Libraries to [2.3] (https://www.drupal.org/project/libraries/releases/7.x-2.3). 
Upgraded Link to [1.4] (https://www.drupal.org/project/link/releases/7.x-1.4). 
Upgraded Views to [3.14] (https://www.drupal.org/project/views/releases/7.x-3.14). 


7.x-1.6

23 December 2015.

Upgraded Chaos Tools Suite to [1.9] (https://www.drupal.org/node/2554183).
Upgraded Features to [2.7] (https://www.drupal.org/node/2592441).
Upgraded Menu Block to [2.7] (https://www.drupal.org/node/2514710).
Upgraded Pathauto to [1.3] (https://www.drupal.org/node/2582001).
Upgraded Pathauto Persist to [1.4] (https://www.drupal.org/node/2575197).
Upgraded Views to [3.13] (https://www.drupal.org/node/2609964).


7.x-1.5
-------

14 May 2015.

* Updated Features to [2.5](https://drupal.org/node/2470129).
* Updated Views to [3.11](https://drupal.org/node/2480259).
* Updated the Image Styles feature to [1.1](https://github.com/misd-service-development/drupal-feature-image-styles/releases/tag/7.x-1.1).
* Updated the Teasers feature to [1.4](https://github.com/misd-service-development/drupal-feature-teasers/releases/tag/7.x-1.4).
* Updated the Cambridge theme to [1.4](https://github.com/misd-service-development/drupal-cambridge-theme/releases/tag/7.x-1.4).

7.x-1.4
-------

19 March 2015.

* Patched the theme with [updated Project Light assets](https://github.com/misd-service-development/drupal-cambridge-theme/commit/c1056076902e87e6eef838adb202321775b2b310).
* Patched the Link module with the fix for issue [#2367069](https://www.drupal.org/node/2367069).
* Patched the Image Styles feature with the fix for issue [#1](https://github.com/misd-service-development/drupal-feature-image-styles/pull/1).
* Updated Chaos Tools to [1.7](https://www.drupal.org/node/2454883) (see also [1.6](https://drupal.org/node/2415979)).
* Updated Features to [2.4](https://drupal.org/node/2446159).
* Updated Menu Block to [2.5](https://www.drupal.org/node/2420375).
* Updated Token to [1.6](https://www.drupal.org/node/2443407).
* Updated Views to [3.10](https://www.drupal.org/node/2424103).

7.x-1.3
-------

13 January 2015.

* Set date formats to UK style.
* Fix menu block configuration for the Cambridge theme.
* Updated the Cambridge theme to [1.3](https://github.com/misd-service-development/drupal-cambridge-theme/releases/tag/7.x-1.3).
* Updated Chaos Tools to [1.5](https://drupal.org/node/2378333).
* Updated Features to [2.3](https://drupal.org/node/2402173).
* Updated Link to [1.3](https://drupal.org/node/2361091).
* Updated Raven to [1.3](https://github.com/misd-service-development/drupal-raven/releases/tag/7.x-1.3).
* Patched the Raven module with the fix for issue [#42](https://github.com/misd-service-development/drupal-raven/pull/42).
* Disabled node previews on new sites.

7.x-1.2
-------

11 September 2014.

* Stopped patched modules from appearing a separate 'patched' directory to stop modules from moving between 'contrib' and 'patched'. You may need to run [Registry Rebuild](https://www.drupal.org/project/registry_rebuild) once.
* Updated Features to [2.2](https://www.drupal.org/node/2316559) (see also [2.1](https://www.drupal.org/node/2311903)).
* Added [Pathauto Persistent State 1.3](https://www.drupal.org/project/pathauto_persist).
* Patched the Menu Firstchild module with the fix for issue [#2295059](https://www.drupal.org/node/2295059).
* Updated Raven to [1.2](https://github.com/misd-service-development/drupal-raven/releases/tag/7.x-1.2).
* Updated the Cambridge theme to [1.2](https://github.com/misd-service-development/drupal-cambridge-theme/releases/tag/7.x-1.2).

7.x-1.1
-------

28 May 2014.

* Updated the teasers feature to [1.3](https://github.com/misd-service-development/drupal-feature-teasers/releases/tag/7.x-1.3) (see also [1.2](https://github.com/misd-service-development/drupal-feature-teasers/releases/tag/7.x-1.2)).
* Updated Chaos Tools to [1.4](https://drupal.org/node/2194551).
* Updated Libraries API to [2.2](https://drupal.org/node/2192173).
* Updated Raven to [1.1](https://github.com/misd-service-development/drupal-raven/releases/tag/7.x-1.1).
* Updated Menu Block to [2.4](https://drupal.org/node/2258261).
* Updated Views to [3.8](https://drupal.org/node/2271305).
* Updated the Cambridge theme to [1.1](https://github.com/misd-service-development/drupal-cambridge-theme/releases/tag/7.x-1.1).

7.x-1.0
-------

27 January 2014.

* Initial release.
